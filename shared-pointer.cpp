#include <iostream>
#include <string>
#include <memory>

using namespace std;

//shared_ptr: If you like sharing (unlike unique_ptr) thats what shared pointers come in.
//shared_ptr works by using REFERENCE COUNTING. In reference counting you keep track of how many references you have to your shared_ptr.
//As the REFERENCE COUNT = 0, thats when the shared_ptr gets deleted.
//Example: I create 1st shared_ptr -> reference count = 1 ; I create 2nd shared_ptr and copy that -> reference count = 2

class Entity
{
public:
    Entity()
    {
        cout << "Create Entity: Constructor!" << endl;
    }
    ~Entity()
    {
        cout << "Destroy Entity: Destructor!" << endl;
    }
    void display()
    {
        cout << "Unique pointer just called me!" << endl;
    }
};

int main()
{
    {
        shared_ptr<Entity> e0;
        {
            //Shared pointer
            shared_ptr<Entity> sharedEntity = make_shared<Entity>();
            cout << "----Reference count = 1----" << endl;

            //make_shared gives you exception safety. Also, shared_ptr has to allocate another block of memory called CONTROL BLOCK where it stores reference count.
            //So if you 1st create "shared_ptr<Entity> sharedEntity(new Entity())" and the 2nd pass it to shared_ptr constructor thats 2 allocation.
            //Fun Fact: You can replace new() and delete() with make_shared<Entity>()

            //Copying shared pointer
            e0 = sharedEntity;
            cout << "----Reference count = 2----" << endl;
        }
        cout << "----Reference count(sharedEntity delete()) = 1----" << endl;

        cout << "----Reference count(e0 delete) = 0----" << endl;
    }

    return 0;
}