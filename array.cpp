#include <iostream>
#include <stdio.h>
#include <string.h>
#include <array>

//syntex: array<obj_type,size> obj;
using namespace std;

int main()
{
    array<int, 4> data_Array = {1, 2, 3, 4};
    array<int, 4> data_Array2 = {5, 6, 7, 8};

    //at() : access array values
    cout << data_Array.at(0) << endl;

    //front(): return first element and back():return last element
    cout << "front(): " << data_Array.front() << endl;
    cout << "back(): " << data_Array.back() << endl;

    //fill(): can fill same value in the whole array
    data_Array.fill(10);

    for (int i = 0; i < data_Array.size(); i++)
    {
        cout << "fill(): " << data_Array[i] << endl;
    }

    //swap: To swap 2 array of same size and same type
    data_Array.swap(data_Array2);

    for (int i = 0; i < data_Array.size(); i++)
    {
        cout << "swap data_Array: " << data_Array[i] << endl;
    }
    for (int i = 0; i < data_Array2.size(); i++)
    {
        cout << "swap data_Array2: " << data_Array2[i] << endl;
    }

    return 0;
}