#include <iostream>
#include <stdio.h>
#include <string.h>

//syntex: pair<string,int> data;
using namespace std;
class Student
{
private:
    string name;
    int age;

public:
    void setStudent(string s, int a)
    {
        name = s;
        age = a;
    }
    void display()
    {
        cout << name << ":" << age << endl;
    }
};

int main()
{
    pair<string, int> p1;
    pair<string, string> p2;
    pair<string, float> p3;
    pair<int, Student> p4;

    //make_pair(): Insert values in pair
    p1 = make_pair("Deepesh", 25);
    p2 = make_pair("dEEPESH", "CHAUHAN");
    p3 = make_pair("Deepesh", 11111.111);

    Student s1;
    s1.setStudent("Deepesh", 21);
    p4 = make_pair(1, s1);

    //To access the pair values use: first and second

    cout << "First Pair" << endl;
    cout << p1.first << ":" << p1.second << endl;
    cout << "Second Pair" << endl;
    cout << p2.first << ":" << p2.second << endl;
    cout << "Third Pair" << endl;
    cout << p3.first << ":" << p3.second << endl;
    cout << "Forth Pair" << endl;
    Student temp = p4.second;
    //cout << p4.first << ":" << temp.display() << endl;
    cout << temp.display << endl;

    return 0;
}