#include <iostream>
#include <iterator>
#include <map>
#include <string>

using namespace std;

int main()
{
    map<string, int> m;

    int n;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        string s;
        cin >> s;
        m[s] = m[s] + 1;
    }
    for (auto itr : m)
    {
        cout << itr.first << ":" << itr.second << endl;
    }

    return 0;
}