#include <iostream>
#include <string>
#include <memory>

using namespace std;

//Unique_ptr: Simplest pointer we have.
//The PROBLEM with unique_ptr : If you want to copy/SHARE that pointer (maybe pass it into a function) then you cant copy the unique pointer.

class Entity
{
public:
    Entity()
    {
        cout << "Create Entity: Constructor!" << endl;
    }
    ~Entity()
    {
        cout << "Destroy Entity: Destructor!" << endl;
    }
    void display()
    {
        cout << "Unique pointer just called me!" << endl;
    }
};

int main()
{
    //Create a unique pointer
    {
        // unique_ptr<Entity> entity(new Entity()); //new Entity(): You have to call the constructor explicitly
        // entity->display();

        //PREFERRED WAY TO CONSTRUCT unique_ptr:
        unique_ptr<Entity> entity = make_unique<Entity>(); //CONSTRUCTOR CALLED
        entity->display();                                 //FUNCTION CALLED
        //Primary reason to use make_unique is due to exception safety. Using make_unique is slightly safer (if constructor happens to thrown an exception)

        //You can't copy a unique_ptr. In unique_pte defination the COPY CONSTRUCTOR and ASSIGNMENT OPERATOR are actually deleted. (Thats why you cant copy).
        // Example:
        unique_ptr<Entity> entity1 = entity; //ERROR

        //DESTRUCTOR CALLED (Automatically)
    }
    return 0;
}