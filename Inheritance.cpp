#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
//Is-a relationship : Public Inheritance
class Car
{
private:
    int gear;

public:
    void incrementGear()
    {
        if (gear < 5)
        {
            gear++;
        }
    }
};
class SportsCar : public Car
{
};

//Here we cant do PUBLIC Inheritance

class Array
{
private:
    int a[10];

public:
    void insert(int index, int value)
    {
        a[index] = value;
    }
};
class Stack : public Array
{
    int top;

public:
    void push(int value)
    {
        insert(top, value);
    }
};

int main()
{
    Stack obj;
    obj.push(10);
    obj.insert(2, 20); //You cannot allow to access insert function. This will be against Stack rule
}