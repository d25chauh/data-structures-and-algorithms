#include <iostream>
#include <stdio.h>
#include <string.h>

//Post: First Assignment then increment
//Pre: First increment and then assignment
using namespace std;
class Integer
{
private:
    int x;

public:
    void setData(int a)
    {
        x = a;
    }
    void showData()
    {
        cout << "x= " << x << endl;
    }
    //Pre Increment
    Integer operator++()
    {
        Integer temp;
        x = x + 1;
        temp.x = x;
        return temp;
    }
    //Post Increment : New Rule- Pass int argument to differentiate between pre and post increment
    Integer operator++(int)
    {
        Integer temp;
        temp.x = x;
        x = x + 1;
        return temp;
    }
};

int main()
{
    Integer i1, i2;
    i1.setData(3);
    i1.showData();
    i2 = ++i1; //i2 = i1.operator++();
    i1.showData();
    i2.showData();

    i2 = i1++; //i2 = i1.operator++(int );
    i1.showData();
    i2.showData();
    return 0;
}