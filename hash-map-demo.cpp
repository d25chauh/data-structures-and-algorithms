#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include <map>
#include <vector>

using namespace std;

int main()
{
    int target = 9;
    vector<int> nums;
    nums.push_back(2);
    nums.push_back(7);
    nums.push_back(11);
    nums.push_back(15);
    vector<int> v;
    unordered_map<int, int> m;
    for (int i = 0; i < nums.size(); i++)
    {
        if (m.find(target - nums[i]) != m.end())
        {
            // Element Found
            v.push_back(m[target - nums[i]]);
            v.push_back(i);
        }
        else
        {
            // key   val -> First
            // 2     0
            m[nums[i]] = i;
        }
    }
    cout << v[0] << " " << v[1];

    return 0;
}