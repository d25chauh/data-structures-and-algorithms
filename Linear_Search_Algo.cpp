#include <iostream>
#include <stdio.h>

using namespace std;

void linearSearch(int a[], int n, int s)
{
    int temp = -1;
    for (int i = 0; i < s; i++)
    {
        if (a[i] == n)
        {
            cout << "Element found " << a[i] << " at index " << i << endl;
            temp = 0;
        }
    }
    if (temp == -1)
    {
        cout << "Element not found" << endl;
    }
}

int main()
{
    int arr[5] = {1, 22, 33, 44, 55};

    int element, size;

    size = sizeof(arr) / sizeof(arr[0]);
    cout << "Enter the element you want to  search:" << endl;
    cin >> element;

    linearSearch(arr, element, size);
    return 0;
}