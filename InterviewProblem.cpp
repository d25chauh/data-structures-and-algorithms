// CPP program to demonstrate multithreading
// using three different callables.
#include <iostream>
#include <vector>
#include <string>
using namespace std;

int f(const int i)
{
    double d = i;
    float f = i;
}
int main()
{
    f(2);
    return 0;
}
// class Vehicle
// {
// public:
//     ~Vehicle()
//     {
//         cout << "Vehicle" << endl;
//     }
// };
// class Engine
// {
// public:
//     ~Engine()
//     {
//         cout << "Engine" << endl;
//     }
// };
// class Truck : public Vehicle
// {
// public:
//     ~Truck()
//     {
//         cout << "Truck" << endl;
//     }

// private:
//     Engine theEngine;
// };
// int main()
// {
//     Truck truck;

//     return 0;
// }

// class Address
// {
// private:
//     string StreetLoc;
//     string City;
//     string State;
//     int ZIP;

// public:
//     Address(const string &street, const string &city, const string &st, const int zip) : StreetLoc(street), City(city), State(st), ZIP(zip) {}

//     //LINE 1
//     // void operator<<(ostream &, const Address &);
//     // friend std::ostream &operator<<(std::ostream &, const Address &);
//     friend std::ostream &operator<<(std::ostream &, const Address &);
// };

// //Line 2
// // void Address::operator<<(ostream &os, const Address &a)
// // std::ostream &Address::operator<<(std::ostream &os, const Address &a)
// std::ostream &Address::operator<<(std::ostream &os, const Address &a)
// {
//     os << "Address is: " << endl;
//     os << a.StreetLoc << endl;
//     os << a.City << "," << a.State << " " << a.ZIP << endl;
//     return os;
// }

// class Polygon
// {
// protected:
//     string name;
//     int num_sides;

// public:
//     Polygon(const string &shapeName, const int sides) : name(shapeName), num_sides(sides)
//     {
//     }
//     virtual ~Polygon();

//     const string &DisplayName() { return name; }
//     int NumSides() { return num_sides; }
//     double Area() { return 0; }
//     double Perimeter() { return 0; }
// };

// class Triangle : public Polygon
// {
// public:
//     Triangle(const string &shapeName, const int sides) : name(shapeName), num_sides(sides) {}
//     virtual ~Triangle();
//     const string &DisplayName() { return name; }
//     int NumSides() { return num_sides; }
//     double Area() { return 0; }
//     double Perimeter() { return 0; }
// };

// class FFT
// {
// public:
//     using SampleData = std::vector<double>;

//     void normalize()
//     {
//         for_each(
//             samples.begin(), samples.end(), [normalization_factor = normalization_factor](double &elem)
//             { elem *= normalization_factor; });
//     }

// private:
//     SampleData samples;
//     double normalization_factor;
// };

// int main()
// {
//     // char *str = new char[6]{'H', 'e', 'l', 'l', 'o', '\0'};

//     // void *p = malloc(5 * sizeof(int));
//     // int *arr_i = new (p) int;

//     // char *str = new char{'H', 'e', 'l', 'l', 'o', '\0'};

//     // string *str = new string[10]("Hello", 5);

//     return 0;
// }

// template <class T>
// class Some
// {
// public:
//     static int stat;
// };

// template <class T>
// int Some<T>::stat = 10;

// int main()
// {
//     Some<int>::stat = 5;
//     cout << Some<int>::stat << " ";
//     cout << Some<float>::stat << " ";
//     cout << Some<long>::stat << " ";

//     return 0;
// }

// // class Buffer
// // {
// //     vector<double> d_arr;

// // // public:
// //     Buffer() = default;
// //     Buffer(size_t s) { d_arr.resize(s); }
// //     Buffer(const Buffer &other)
// //     {
// //         d_arr = other.d_arr;
// //     }
// //     Buffer(Buffer &&other)
// //     {
// //         cout << "Calling Move Constructor" << endl;
// //         d_arr = std::move(other.d_arr);
// //     }
// // };

// class Person
// {
//     string *namePtr;

// public:
//     Person()
//     {
//         namePtr = new string;
//     }
//     void setName(string s)
//     {
//         *namePtr = s;
//         cout << "namePtr:" << namePtr << endl;
//     }
//     void showName(Person &obj)
//     {
//         cout << *(obj.namePtr) << endl;
//     }
// };

// int main()
// {

//     Person president;
//     Person chairman;

//     president.setName("Tom");
//     chairman = president;

//     president.setName("Janet");

//     chairman.showName(chairman);
//     president.showName(president);
//     return 0;
// }
