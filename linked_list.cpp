//Linked list operation we will be implementing:
/*
    1. appendNode()
    2. prependNode()
    3. insertNodeAfter()
    4. deleteNodeByKey()
    5. updatenodeByKey()
    6. print()
*/
#include <iostream>
#include <stdio.h>

using namespace std;

class Node
{
private:
public:
    int key;
    int data;
    Node *next;
    Node()
    {
        key = 0;
        data = 0;
        next = NULL;
    }
    Node(int k, int d)
    {
        key = k;
        data = d;
    }
};

class SingleLinkList
{
public:
    Node *head;
    SingleLinkList()
    {
        head = NULL;
    }
    //We pass the address of the head node to Constructor
    SingleLinkList(Node *n)
    {
        cout << "Paremeterized Constructor: Head is initialized" << endl;
        head = n;
    }

    //If a node exist or not - Traversal of LinedList
    Node *nodeExist(int k)
    {
        Node *temp = NULL;

        Node *ptr = head;
        while (ptr != NULL)
        {
            if (ptr->key == k)
            {
                temp = ptr;
            }
            ptr = ptr->next;
        }
        return temp;
    }

    //Append a Node to the LinkedList - Append at the end of the List
    void appendNode(Node *n)
    {
        if (nodeExist(n->key) != NULL)
        {
            cout << "Node Already Exist" << endl;
        }
        else
        {
            if (head == NULL)
            {
                head = n;
                cout << "Appending the First node to LinkedList" << endl;
            }
            else
            {
                //Traverse through the linked list

                //ptr pointing to HEAD node
                Node *ptr = head;
                while (ptr->next != NULL)
                {
                    //ptr is now pointing to next node
                    ptr = ptr->next;
                }
                //Pass by address: The address you pass is permanent
                ptr->next = n;
                cout << "Node Appended Successfully" << endl;
            }
        }
    }

    //prependNode - Attach the node at the start
    void prependNode(Node *n)
    {
        if (nodeExist(n->key) != NULL)
        {
            cout << "Node Already Exist!" << endl;
        }
        else
        {
            n->next = head;
            head = n;
            cout << "Node Successfully Prepended" << endl;
        }
    }

    //insertNode - Insert a node between any node

    void insertNode(Node *n, int k)
    {
        // Node *ptr = head;

        // while (ptr->key != k)
        // {
        //     ptr = ptr->next;
        // }
        // n->next = ptr->next;
        // ptr->next = n;

        //Directly point to the node after which you want to insert
        Node *ptr = nodeExist(k);
        if (ptr == NULL)
        {
            cout << "Node after which you want to insert newNode, doesnt exist" << endl;
        }
        else
        {
            if (nodeExist(n->key) != NULL)
            {
                cout << "Node key you want to add already exist. Add node with different key" << endl;
            }
            else
            {
                n->next = ptr->next;
                ptr->next = n; //n is address of newNode passed
                cout << "Node Inserted Successfully" << endl;
            }
        }
    }

    //deleteNode - Delaete a node with perticular key

    void deleteNode(int k)
    {
        if (head == NULL)
        {
            cout << "LinkedList is empty! Cannot delete." << endl;
        }
        else if (head != NULL)
        {
            //Deleting the head node
            if (head->key == k)
            {
                head = head->next;
                cout << "Head is deleted and we have a new Head node" << endl;
            }
            else
            {
                Node *ptr = NULL;
                Node *prevPtr = head;
                Node *currentPtr = head->next;

                while (currentPtr != NULL)
                {
                    if (currentPtr->key == k)
                    {
                        ptr = currentPtr;
                        currentPtr = NULL; //Exit the while loop
                    }
                    else
                    {
                        prevPtr = prevPtr->next;
                        currentPtr = currentPtr->next;
                    }
                }
                if (ptr != NULL)
                {
                    prevPtr->next = ptr->next;
                    cout << "Node at key " << k << " unlinked!" << endl;
                }
                else
                {
                    cout << "Node doesnt exist with key value: " << k << endl;
                }
            }
        }
    }

    void updateNode(int k, int d)
    {
        Node *ptr = nodeExist(k);
        if (ptr != NULL)
        {
            ptr->data = d;
            cout << "Node is Updated successfully" << endl;
        }
        else
        {
            cout << "node does not exist" << endl;
        }
    }

    void print()
    {
        if (head == NULL)
        {
            cout << "No node exist" << endl;
        }
        else
        {
            cout << "Linked List Values are:" << endl;
            Node *ptr = head;

            while (ptr != NULL)
            {
                cout << "(" << ptr->key << " , " << ptr->data << ") -->";
                ptr = ptr->next;
            }
        }
    }
};

int main()
{
    SingleLinkList s;
    int key1, k1, data1;
    int option;

    do
    {
        cout << endl
             << "Enter a operation (0 for exit):" << endl;
        cout << "1. appendNode()" << endl;
        cout << "2. prependNode()" << endl;
        cout << "3. insertNodeAfter()" << endl;
        cout << "4. deleteNodeByKey()" << endl;
        cout << "5. updatenodeByKey()" << endl;
        cout << "6. print()" << endl;

        cin >> option;

        //Dynamic memory allocation
        Node *n1 = new Node();

        switch (option)
        {
        case 0:
            break;
        case 1:
            cout << "Append Node operation." << endl;
            cout << "Enter key & data of the Node to be appended:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.appendNode(n1);
            break;
        case 2:
            cout << "Prepend Node operation." << endl;
            cout << "Enter key & data of the Node to be appended:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.prependNode(n1);
            break;
        case 3:
            cout << "Insert Node after a node with key K." << endl;
            cout << "Please enter key K after which you want to insert the New Node: " << endl;
            cin >> k1;
            cout << "Enter key & data of the new Node to be inserted:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.insertNode(n1, k1);
            break;
        case 4:
            cout << "Delete Node at key K." << endl;
            cout << "Enter key of the Node to be deleted" << endl;
            cin >> k1;
            s.deleteNode(k1);
            break;
        case 5:
            cout << "Update node" << endl;
            cout << "Enter key & data of the node that needs to be updated:" << endl;
            cin >> k1;
            cin >> data1;
            s.updateNode(k1, data1);
            break;
        case 6:
            cout << "Displaying all the elements" << endl;
            s.print();
            break;
        default:
            cout << "Invalid Option" << endl;
        }
    } while (option != 0);

    return 0;
}