#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <iterator>

using namespace std;
int main()
{
    vector<int> input;
    cout << "Initial capacity: " << input.capacity() << endl;
    cout << "Initial size: " << input.size() << endl;
    for (int i = 0; i < 10; i++)
    {
        //push_back()
        input.push_back(i);
    }

    cout << "Capacity after push_back(): " << input.capacity() << endl;
    cout << "Size after push_back(): " << input.size() << endl;

    // for (int j = 0; j < 10; j++)
    // {
    //     //pop_back()
    //     input.pop_back();
    // }

    // cout << "Capacity after pop_back: " << input.capacity() << endl;
    // cout << "Size after pop_back: " << input.size() << endl;

    //front() and back(): return first and last value

    //iterator
    vector<int>::iterator it = input.begin();
    input.insert(it + 3, 3);

    for (int i = 0; i < input.size(); i++)
    {
        cout << input[i] << endl;
    }

    //Vector of pairs
    vector<pair<int, int> > v_p = {{1, 2}, {2, 3}, {4, 5}};
    vector<pair<int, int> >::iterator it2;
    for (it2 = v_p.begin(); it2 != v_p.end(); it2++)
    {
        cout << it2->first << ":" << it2->second << endl;
    }

    return 0;
}