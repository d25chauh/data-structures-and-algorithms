#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

//1. Constructor can only be instance member function and cannot be static.
//2. Consructor solves the problem of initialization.
class Complex
{
private:
    int a, b;

public:
    //Default constructor
    Complex()
    {
        cout << "Default Constructor is called" << endl;
        a = 0;
        b = 0;
    }
    //Parameterized Constructor
    Complex(int x, int y)
    {
        cout << "Parameterized Constructor is called" << endl;
        a = x;
        b = y;
    }
    //Constructor overloading
    Complex(int z)
    {
        cout << "Overloaded Constructor" << endl;
        a = z;
    }
    //Copy Constructor: If we write Complex(Complex c) then this will lead to RECURSION
    Complex(Complex &c)
    {
        cout << "Copy Constructor is called" << endl;
        a = c.a;
        b = c.b;
    }
};

int main()
{
    Complex obj;        //Defaut constructor is called
    Complex obj1(3, 4); //Parameterized Constructor is called
    Complex obj2(3);    //Overload Constructor is called

    Complex obj3(obj1); //Default Copy Constructor

    return 0;
}