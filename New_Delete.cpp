#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
// **Static Memory Allocation:** int x = Memory to be allocated is decided in compile time but actual memory is allocated when the program starts running on RAM.
// During execution of the program you cannot alter this static memory. Memory flexibility is not allowed.
// **Dynamic Memory Allocation:** Compiler dont know about these variables. These variables are decided on the runtime.

//Use new and delete keyword to create Dynamic memory. Example: float *p= new float[5]; delete []p;