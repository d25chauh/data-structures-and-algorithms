#include <iostream>
#include <queue>
#include <stdio.h>

using namespace std;

class CircularQueue
{
private:
    int rear;
    int front;
    int arr[4];

public:
    CircularQueue()
    {
        rear = -1;
        front = -1;
        for (int i = 0; i < 5; i++)
        {
            arr[i] = 0;
        }
    }

    bool isEmpty()
    {
        if (rear == -1 && front == -1)
        {
            cout << "Circular Queue is Empty" << endl;
            return true;
        }
        else
        {
            return false;
        }
    }

    bool isFull()
    {
        if (front == (rear + 1) % 5)
        {
            cout << "The Circular Queue is FULL." << endl;
            return true;
        }
        else
        {
            cout << "Circular Queue is NOT Full" << endl;
            return false;
        }
    }

    void enqueue(int val)
    {
        if (isFull())
        {
            cout << "Circular Queue is Full! Cant add elements." << endl;
            return;
        }
        else if (isEmpty())
        {
            cout << "The Circular Queue was empty. Adding first element." << endl;
            rear = 0;
            front = 0;
            arr[rear] = val;
        }
        else
        {
            rear = (rear + 1) % 5;
            arr[rear] = val;
        }
    }

    int dequeue()
    {
        int x;
        if (isEmpty())
        {
            cout << "Cannot remove elements as Circular Queue is empty" << endl;
            return 0;
        }
        else if (front == rear)
        {
            x = arr[front];
            arr[front] = 0;
            rear = -1;
            front = -1;
        }
        else
        {
            x = arr[front];
            arr[front] = 0;
            front = (front + 1) % 5;
        }
        return x;
    }

    void display()
    {
        for (int i = 0; i < 5; i++)
        {
            cout << arr[i] << " ";
        }
    }
};

int main()
{

    CircularQueue q;

    int op, value, a;

    do
    {
        cout << endl;
        cout << "Enter a operation (0 for exit):" << endl;
        cout << "1. Enqueue" << endl;
        cout << "2. Dequeue" << endl;
        cout << "3. isEmpty()" << endl;
        cout << "4. isFull()" << endl;
        cout << "5. Display" << endl;

        cin >> op;

        switch (op)
        {
        case 0:
            break;
        case 1:
            cout << "Enter the val you want to push: ";
            cin >> value;
            q.enqueue(value);
            break;
        case 2:
            a = q.dequeue();
            cout << "The element that is removed is: " << a << endl;
            break;
        case 3:
            cout << q.isEmpty() << endl;
            break;
        case 4:
            cout << q.isFull() << endl;
            break;
        case 5:
            q.display();
            break;
        default:
            cout << "Invalid input.\n";
            break;
        }
    } while (op != 0);

    return 0;
}
