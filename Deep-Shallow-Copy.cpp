#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

class Dummy
{
private:
    int a, b;
    int *p;

public:
    Dummy()
    {
        p = new int();
    }
    void setData(int x, int y, int z)
    {
        a = x;
        b = y;
        *p = z;
    }
    void showData()
    {
        cout << a << ":" << b << ":" << *p << endl;
    }
    Dummy(Dummy &d)
    {
        //Shallow copy
        a = d.a;
        b = d.b;

        //Deep Copy:  Copying Memory resource also
        p = new int();
        *p = *(d.p);
    }
    ~Dummy()
    {
        delete p;
    }
};

int main()
{
    Dummy obj1;
    obj1.setData(1, 2, 3);
    Dummy obj2 = obj1; //Copy constructor is called
    obj2.showData();
    return 0;
}