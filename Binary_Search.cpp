#include <iostream>
#include <stdio.h>

using namespace std;

int main()
{
    int left, right, mid, ele;

    int arr[10] = {1, 3, 5, 7, 9, 11, 13, 14, 15, 17};

    int size_arr = (sizeof(arr) / sizeof(arr[0])) - 1;

    left = 0;
    right = size_arr - 1;

    cout << "Enter the element you want to search: ";
    cin >> ele;

    bool found = false;

    while (left <= right)
    {
        mid = left + (right - left) / 2;

        if (arr[mid] == ele)
        {
            cout << "Element found at index " << mid << endl;
            found = true;
            return mid;
        }

        else if (ele > arr[mid])
        {
            left = mid + 1;
        }
        else if (ele < arr[mid])
        {
            right = mid - 1;
        }
    }
    if (found != true)
    {
        cout << "Element not exist" << endl;
    }

    return 0;
}