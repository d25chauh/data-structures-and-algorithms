#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
class Parent
{
private:
    int a;

public:
    //Constructor
    Parent(int z)
    {
        cout << "Parent class constructor" << endl;
        a = z;
    }
    ~Parent()
    {
        cout << "Called Parent Destructor" << endl;
    }
};
class Child : public Parent
{
private:
    int b;

public:
    //Constructor
    Child(int x, int y) : Parent(x)
    {
        cout << "Child class constructor" << endl;
        b = y;
    }
    ~Child()
    {
        cout << "Called Child Destructor" << endl;
    }
};

int main()
{
    Child obj(2, 3);

    return 0;
}