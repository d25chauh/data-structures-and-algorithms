#include <iostream>
#include <stdio.h>
#include <stack>

using namespace std;

int main()
{
    string s = "[{(]}]";
    stack<char> st;
    bool result = true;

    for (auto i : s)
    {
        if (i == '(' || i == '[' || i == '{')
        {
            st.push(i);
            cout << "Push: " << i << endl;
        }
        else
        {
            if (st.empty() || (st.top() == '(' && i != ')') || (st.top() == '{' && i != '}') || (st.top() == '[' && i != ']'))
            {
                result = false;
            }
            st.pop();
        }
    }
    cout << "Result:" << result << endl;
    return 0;
}