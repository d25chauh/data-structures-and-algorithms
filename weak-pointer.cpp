#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Entity
{
public:
    Entity()
    {
        cout << "Create Entity: Constructor!" << endl;
    }
    ~Entity()
    {
        cout << "Destroy Entity: Destructor!" << endl;
    }
    void display()
    {
        cout << "Unique pointer just called me!" << endl;
    }
};

int main()
{
    {
        //Weak_ptr is good if you dont want to take ownership of the entity.
        //With weak_ptr you can check if this entity is alive but weak_ptr won't keep it alive (because it actually does not increase the reference count).
        //You can ask a weak_ptr: 1) Are you expired? 2) Are you Valid?
        weak_ptr<Entity> weakEntity;
        {
            //shared_ptr
            shared_ptr<Entity> sharedEntity = make_shared<Entity>();

            // weak_ptr does kinda the same as if you were copying the shared entity and increase the reference count
            // EXCEPT weak_ptr does not increase the REFERENCE COUNT.
            // Example: When you assign a shared_ptr to a shared_ptr : It will increase the reference count.
            // However, When you assign a shared_ptr to a weak_ptr : It will NOT increase the reference count
            weakEntity = sharedEntity;
            sharedEntity->display();
        }
    }

    return 0;
}