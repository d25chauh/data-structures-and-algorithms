#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
class Box
{
private:
    int l, b, h;

public:
    //this pointer =  local object pointer and Present is every member function of class.
    void set_dimensions(int l, int b, int h)
    {
        this->l = l;
        this->b = b;
        this->h = h;
    }
    void show_dimensions()
    {
        cout << l << ":" << b << ":" << h << endl;
    }
};

int main()
{
    //Object pointer
    Box *obj = new Box();
    obj->set_dimensions(1, 2, 3);
    obj->show_dimensions();
    return 0;
}