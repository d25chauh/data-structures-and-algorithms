// Binary Search Tree as Dynamic Node in memory
#include <iostream>
#include <stdio.h>
#define SPACE 10

using namespace std;

class TreeNode
{
public:
    int value;
    TreeNode *left;
    TreeNode *right;
    TreeNode()
    {
        value = 0;
        left = NULL;
        right = NULL;
    }
    TreeNode(int v)
    {
        value = v;
        left = NULL;
        right = NULL;
    }
};

class BST
{
public:
    TreeNode *root;

    bool isEmpty()
    {
        if (root == NULL)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void InsertNode(TreeNode *new_node)
    {
        if (root == NULL)
        {
            // If there is no root node then the new node will become the roor node
            root = new_node;
        }
        else
        {
            TreeNode *temp;
            temp = root; // temp will have the address of root node

            // Traverse through the whole tree
            while (temp != NULL)
            {
                // Condition 1: If new_node value == temp value
                if (new_node->value == temp->value)
                {
                    return; // Duplicate
                }

                // Condition 2: If new_node value < temp value and LST is NULL then add the new_node's address to LST
                else if (new_node->value < temp->value && temp->left == NULL)
                {

                    temp->left = new_node;
                    cout << "Value inserted to Left Sub Tree" << endl;
                    return;
                }

                // Condition3: If new_node value < temp value then temp is moved to next left node
                else if (new_node->value < temp->value)
                {
                    // temp is next node in Left Sub Tree
                    cout << "Node point to next node in Left Sub Tree" << endl;
                    temp = temp->left;
                }

                // Condition 4: If new_node > temp value and RST is NULL then add new_node to RST
                else if (new_node->value > temp->value && temp->right == NULL)
                {
                    temp->right = new_node;
                    cout << "Value inserted to Right Sub Tree" << endl;
                    return;
                }

                // Condition 5: if new_node > temp THEN temp will move to next node in Right Sub Tree
                else
                {
                    cout << "Node point to next node in Right Sub Tree" << endl;
                    temp = temp->right;
                }
            }
        }
    }

    // Tree Traversal (Tree Search) Techniques in BT and BST
    void printPreOrder(TreeNode *r)
    {
        if (r == NULL)
        {
            return;
        }
        cout << r->value << " - ";
        printPreOrder(r->left);  // recurce to left subtree [Pause and Continue]
        printPreOrder(r->right); // recurse to right subtree [Pause and Continue]
    }

    // Print In-Order Traversal elements
    void printInOrder(TreeNode *r)
    {
        if (r == NULL)
        {
            return;
        }
        printInOrder(r->left); // recurce to left subtree [Pause and Continue]
        cout << r->value << " - ";
        printInOrder(r->right); // recurse to right subtree [Pause and Continue]
    }
    // Print Post-Order Traversal elements
    void printPostOrder(TreeNode *r)
    {
        if (r == NULL)
        {
            return;
        }
        printPostOrder(r->left);  // recurce to left subtree [Pause and Continue]
        printPostOrder(r->right); // recurse to right subtree [Pause and Continue]
        cout << r->value << " - ";
    }

    // Print tree
    void printTree(TreeNode *r, int space)
    {
        if (r == NULL)
        {
            return;
        }
        space += SPACE;
        printTree(r->right, space);
        cout << endl;
        for (int i = SPACE; i < space; i++)
        {
            cout << " ";
        }
        cout << r->value << '\n';
        printTree(r->left, space);
    }
};

int main()
{
    int option, val;
    BST obj;
    do
    {
        cout << "Enter the operation (0 to exit):" << endl;
        cout << "1: Insert Node" << endl;
        cout << "2: Search Node (Tree Traversal)" << endl;
        cout << "3: Delete Node" << endl;
        cout << "4: Print BST values" << endl;
        cout << "5: Clear Screen" << endl;
        cout << "0: Exit Program" << endl;

        cin >> option;
        TreeNode *new_node = new TreeNode();

        switch (option)
        {
        case 0:
            break;
        case 1:
            cout << "Insert" << endl;
            cout << "Enter the value of new Tree Node to be inserted in the BST" << endl;
            cin >> val;
            new_node->value = val;
            obj.InsertNode(new_node);
            break;
        case 2:
            cout << "Search" << endl;
            break;
        case 3:
            cout << "Delete" << endl;
            break;
        case 4:
            cout << "Print and Traverse" << endl;
            obj.printTree(obj.root, 5);
            cout << endl;
            obj.printPreOrder(obj.root);
            cout << endl;
            obj.printInOrder(obj.root);
            cout << endl;
            obj.printPostOrder(obj.root);
            cout << endl;
            break;
        case 5:
            cout << "clrscr" << endl;
            system("cls");
            break;
        default:
            cout << "invalid option" << endl;
            break;
        }
    } while (option != 0);

    return 0;
}