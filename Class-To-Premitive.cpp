#include <iostream>
#include <stdio.h>
#include <string.h>

//Solution: Use casting operator()
using namespace std;
class Complex
{
private:
    int a, b;

public:
    //Casting operator()
    operator int()
    {
        return (a);
    }

    void setData(int x, int y)
    {
        a = x;
        b = y;
    }
    void showData()
    {
        cout << a << ":" << b << endl;
    }
};

int main()
{
    Complex c1;
    c1.setData(3, 4);
    c1.showData();
    int x;
    //Casting operator is called

    x = c1; //Means: x = c1.operator int();

    cout << "x: " << x << endl;

    return 0;
}