#include <iostream>
#include <stdio.h>
#include <stack>

using namespace std;

class Queue
{
private:
    int rear;
    int front;
    int arr[4];

public:
    Queue()
    {
        rear = -1;
        front = -1;
        for (int i = 0; i < 5; i++)
        {
            arr[i] = 0;
        }
    }

    bool isEmpty()
    {
        if (rear == -1 && front == -1)
        {
            cout << "The queue is Empty" << endl;
            return true;
        }
        else
        {
            return false;
        }
    }

    bool isFull()
    {
        if (front == ((rear + 1) % 5))
        {
            cout << "The queue is full" << endl;
            return true;
        }
        else
        {
            return false;
        }
    }

    void enqueue(int val)
    {
        if (isFull())
        {
            cout << "Circular queue is full." << endl;
            return;
        }
        else if (isEmpty())
        {
            cout << "Add the first element in the circular queue" << endl;
            rear = 0;
            front = 0;
            arr[front] = val;
        }
        else
        {
            cout << "Adding element to next rear index 4: " << 4 << endl;
            rear = (rear + 1) % 5;
            arr[rear] = val;
        }
    }

    int dequeue()
    {
        int x;
        if (isEmpty())
        {
            return 0;
        }
        else if (front == rear)
        {
            x = arr[front];
            arr[front] = 0;
            front = -1;
            rear = -1;
        }
        else
        {
            x = arr[front];
            arr[front] = 0;
            front = (front + 1) % 5;
        }
        return x;
    }

    void display()
    {
        for (int i = 0; i < 5; i++)
        {
            cout << arr[i] << " ";
        }
    }
};

int main()
{
    Queue q;
    int op, value, a;

    do
    {
        cout << endl;
        cout << "Enter a operation (0 for exit):" << endl;
        cout << "1. Enqueue" << endl;
        cout << "2. Dequeue" << endl;
        cout << "3. isEmpty()" << endl;
        cout << "4. isFull()" << endl;
        cout << "5. Display" << endl;

        cin >> op;

        switch (op)
        {
        case 0:
            break;
        case 1:
            cout << "Enter the val you want to push: ";
            cin >> value;
            q.enqueue(value);
            break;
        case 2:
            a = q.dequeue();
            cout << "The element that is removed is: " << a << endl;
            break;
        case 3:
            cout << q.isEmpty() << endl;
            break;
        case 4:
            cout << q.isFull() << endl;
            break;
        case 5:
            q.display();
            break;
        default:
            cout << "Invalid input.\n";
            break;
        }
    } while (op != 0);

    return 0;
}