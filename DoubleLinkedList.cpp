#include <iostream>
#include <stdio.h>

using namespace std;

class Node
{
public:
    int key;
    int data;
    Node *next;
    Node *prev;

    Node()
    {
        key = 0;
        data = 0;
        next = NULL;
        prev = NULL;
    }
    Node(int k, int d)
    {
        key = k;
        data = d;
    }
};

class DoubleLinkedList
{
public:
    Node *head;

    DoubleLinkedList()
    {
        head = NULL;
    }
    DoubleLinkedList(Node *n)
    {
        head = n;
    }

    Node *nodeExist(int k)
    {
        Node *temp = NULL;

        Node *ptr = head;
        while (ptr != NULL)
        {
            if (ptr->key == k)
            {
                temp = ptr;
            }
            ptr = ptr->next;
        }
        return temp;
    }

    void appendNode(Node *n)
    {
        if (nodeExist(n->key) != NULL)
        {
            cout << "Node Already exist! Cannot append" << endl;
        }
        else
        {
            if (head == NULL)
            {
                cout << "This is the first element of the Double Linked List" << endl;
                head = n;
            }
            else
            {
                Node *ptr = head;
                while (ptr->next != NULL)
                {
                    ptr = ptr->next;
                }
                ptr->next = n;
                n->prev = ptr;
                cout << "Node Appended Successfully" << endl;
            }
        }
    }

    void prependNode(Node *n)
    {
        if (nodeExist(n->key) != NULL)
        {
            cout << "Node Already exist! Cannot append" << endl;
        }
        else
        {
            Node *ptr = head;

            ptr->prev = n;
            n->next = ptr;
            head = n;
            cout << "Node Prepended Successfully" << endl;
        }
    }

    void insertNodeAfter(Node *n, int k)
    {
        Node *ptr = nodeExist(k);
        Node *nextNode = ptr->next;

        if (ptr == NULL)
        {
            cout << "No node exist with key" << endl;
        }
        else
        {
            if (nodeExist(n->key) != NULL)
            {
                cout << "NODE ALREADY EXIST WITH SAME KEY" << endl;
            }
            else
            {
                if (nextNode == NULL)
                {
                    n->prev = ptr;
                    ptr->next = n;
                    cout << "Node Inserted Successfully in the end" << endl;
                }
                else
                {
                    n->next = nextNode;
                    nextNode->prev = n;

                    ptr->next = n;
                    n->prev = ptr;
                    cout << "Node Inserted Successfully" << endl;
                }
            }
        }
    }

    void deleteNode(int k)
    {
        if (head == NULL)
        {
            cout << "Cannot delete Node because Linked List is Empty" << endl;
        }
        else
        {
            if (head->key == k)
            {
                head = head->next;
                cout << "Head is deleted and we have a new Head node" << endl;
            }
            else
            {
                Node *previousNode = head;
                Node *currentNode = head->next;

                Node *ptr = NULL;

                while (currentNode != NULL)
                {
                    if (currentNode->key == k)
                    {
                        ptr = currentNode;
                        currentNode = NULL;
                        cout << "Delete of Current Node successful" << endl;
                    }
                    else
                    {
                        previousNode = previousNode->next;
                        currentNode = currentNode->next;
                    }
                }
                if (ptr != NULL)
                {
                    previousNode->next = ptr->next;
                    ptr = ptr->next;
                    ptr->prev = previousNode;

                    cout << "Node at key " << k << " unlinked!" << endl;
                }
                else
                {
                    cout << "Node doesnt exist with key value: " << k << endl;
                }
            }
        }
    }

    void updateNode(int k, int d)
    {
        Node *location = nodeExist(k);
        if (location != NULL)
        {
            location->data = d;
            cout << "Node is Updated successfully" << endl;
        }
        else
        {
            cout << "Node with key " << k << " not found" << endl;
        }
    }

    void print()
    {
        if (head == NULL)
        {
            cout << "No node exist" << endl;
        }
        else
        {
            cout << "Linked List Values are:" << endl;
            Node *ptr = head;

            while (ptr != NULL)
            {
                cout << "(" << ptr->key << " , " << ptr->data << ") -->";
                ptr = ptr->next;
            }
        }
    }
};

int main()
{
    DoubleLinkedList s;
    int key1, k1, data1;
    int option;

    do
    {
        cout << endl
             << "Enter a operation (0 for exit):" << endl;
        cout << "1. appendNode()" << endl;
        cout << "2. prependNode()" << endl;
        cout << "3. insertNodeAfter()" << endl;
        cout << "4. deleteNodeByKey()" << endl;
        cout << "5. updatenodeByKey()" << endl;
        cout << "6. print()" << endl;

        cin >> option;

        //Dynamic memory allocation
        Node *n1 = new Node();

        switch (option)
        {
        case 0:
            break;
        case 1:
            cout << "Append Node operation." << endl;
            cout << "Enter key & data of the Node to be appended:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.appendNode(n1);
            break;
        case 2:
            cout << "Prepend Node operation." << endl;
            cout << "Enter key & data of the Node to be appended:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.prependNode(n1);
            break;
        case 3:
            cout << "Insert Node after a node with key K." << endl;
            cout << "Please enter key K after which you want to insert the New Node: " << endl;
            cin >> k1;
            cout << "Enter key & data of the new Node to be inserted:" << endl;
            cin >> key1;
            cin >> data1;
            n1->key = key1;
            n1->data = data1;
            s.insertNodeAfter(n1, k1);
            break;
        case 4:
            cout << "Delete a node. Please input key k:" << endl;
            cin >> key1;
            s.deleteNode(key1);
            break;
        case 5:
            cout << "Update a Node" << endl;
            cout << "Enter the key and data: " << endl;
            cin >> key1;
            cin >> data1;
            s.updateNode(key1, data1);
            break;
        case 6:
            cout << "Displaying all the elements" << endl;
            s.print();
            break;
        default:
            cout << "Invalid Option" << endl;
        }
    } while (option != 0);

    return 0;
}