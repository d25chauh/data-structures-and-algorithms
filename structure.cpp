#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

//Memory consumed before using the struct : 0 bytes (since we have not used this struct yet)
//We can define the function inside the struct (in C++)
struct Book //GLOBAL Structure
{

private:
    //Member variable
    char title[20];
    int bookId;
    float price;

public:
    //Member functions
    void input()
    {
        cout << "Enter the input: " << endl;
        cin >> bookId >> price >> title;
    }
    void display()
    {
        cout << title << " : " << bookId << " : " << price << endl;
    }
};

int main()
{
    //Memory consumed : 28 bytes (20 + 4 + 4)
    // Book obj = {"C++ Book", 12345, 22.5};
    Book obj;
    obj.input();
    obj.display();

    return 0;
}