#include <iostream>
#include <stdio.h>
#include <map>

using namespace std;

//MAP is an associative array
//MAP contains sorted key-value pairs

int main()
{
    map<int, string> customer;
    customer[100] = "Deepesh";
    customer[123] = "Rishab";
    customer[145] = "Shrey";
    customer[171] = "Rashi";
    customer[200] = "Bhar";

    // map<int, string> map{
    //     {100, "Deepesh"},
    //     {123, "Rishab"},
    //     {145, "Shrey"},
    //     {171, "Rashi"},
    //     {200, "Bhar"}};

    //To display all the map values

    cout << "The size of the hashmap is: " << customer.size() << endl;

    //0 : If not empty
    //1: If empty
    cout << "Check if the hasmap is empty of not: " << customer.empty() << endl;

    //Insert functionality
    cout << "Insert into the hashmap" << endl;
    //You have to make a pair to insert the values
    customer.insert(pair<int, string>(205, "new Member"));

    map<int, string>::iterator it = customer.begin();
    while (it != customer.end())
    {
        cout << it->first << " : " << it->second << endl;
        it++;
    }

    //Clear the map
    cout << "Clear the map" << endl;
    customer.clear();

    return 0;
}
