#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
class Car
{
    //*_vptr is created by compiler
public:
    //(Runtime Binding) Late Binding using Virtual keyword
    virtual void shiftGear()
    {
        cout << "Called Car::shiftGear()" << endl;
    }
};
class sportsCar : public Car
{
public:
    //Method Overriding: Function Prototype exactly same but defination is different
    void shiftGear()
    {
        //Virtual Function: Since shiftGear() in parent class is Virtual
        cout << "Called sportsCar::shiftGear()" << endl;
    }
};
int main()
{
    Car *obj = new sportsCar; //This address is allocated at runtime and not compile time
    obj->shiftGear();         //Early Binding : Call Class Car shiftGear(). Use virtual function to overcome this problem

    //If we use virtual: Pointer's type is not considered insted pointer's address is considered.
    return 0;
}