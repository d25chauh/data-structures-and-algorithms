#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
class Car
{
public:
    void shiftGear()
    {
        cout << "Called parent::shiftGear()" << endl;
    }
    void f2()
    {
        cout << "Called parent::f2()" << endl;
    }
};
class sportsCar : public Car
{
public:
    //Method Overriding: Function Prototype exactly same but defination is different
    void shiftGear()
    {
        cout << "Called sportsCar::shiftGear()" << endl;
    }
    //Method Hiding: Function prototypes are different
    void f2(int x)
    {
        cout << "Called Chile::f2(int)" << endl;
    }
};
int main()
{
    sportsCar obj;
    obj.shiftGear(); //Which shiftGear() is called? Called Early Binding: Compiler will check obj type and calle sportsCar->shiftGear().

    //obj.f2();        //Call f2() of sportsCar. Since the signature is not matching: ERROR

    obj.f2(4); //Called sportsCar->f2(int)
    return 0;
}