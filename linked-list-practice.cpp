#include <iostream>
#include <stdio.h>

using namespace std;

class Node
{
public:
    int key;
    int data;
    Node *next;
    Node()
    {
        key = 0;
        data = 0;
        next = NULL;
    }
    Node(int k, int d)
    {
        key = k;
        data = d;
        next = NULL;
    }
};

class SingleLinkedList
{
public:
    Node *head;

    Node *nodeExist(int k)
    {
        Node *result = NULL;
        Node *ptr = head;

        while (ptr != NULL)
        {
            if ((ptr->key) == k)
            {
                cout << "Node Exist" << endl;
                result = ptr;
            }
            ptr = ptr->next;
        }
        return result;
    }

    SingleLinkedList()
    {
        head = NULL;
    }
    SingleLinkedList(Node *n)
    {
        head = n;
    }
    void prependNode(Node *n)
    {
        n->next = head;
        head = n;
    }
    void appendNode(Node *n)
    {
        head->next = n;
        n = NULL;
    }
};

int main()
{
    Node n1(1, 10);
    Node n2(2, 20);
    Node n3(3, 30);

    SingleLinkedList s(&n1);
    s.appendNode(&n2);
    s.prependNode(&n3);
    s.nodeExist(1);

    return 0;
}