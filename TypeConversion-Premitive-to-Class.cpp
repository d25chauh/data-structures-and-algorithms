#include <iostream>
#include <stdio.h>
#include <string.h>

//Solution: Using Constructor
using namespace std;
class Complex
{
private:
    int a, b;

public:
    Complex()
    {
        //Default constructor
    }
    Complex(int k)
    {
        a = k;
        b = 0;
    }
    void setData(int x, int y)
    {
        a = x;
        b = y;
    }
    void showData()
    {
        cout << a << ":" << b << endl;
    }
};

int main()
{
    Complex c1;
    int x = 5;
    c1 = x; //Can be seen as: Complex c1(int)
    c1.showData();
    return 0;
}