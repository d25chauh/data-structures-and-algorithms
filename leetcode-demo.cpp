#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

int main()
{
    int arr[9] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    vector<int> nums;

    for (int i = 0; i < 9; i++)
    {
        nums.push_back(arr[i]);
    }
    int maxSum = nums[0];
    int curr_sum = 0;

    for (int i : nums)
    {
        if (curr_sum < 0)
        {
            cout << "Current Sum is set to Zero" << endl;
            curr_sum = 0;
        }
        curr_sum += i;
        cout << "Current Sum = " << curr_sum << endl;
        maxSum = max(maxSum, curr_sum);
        cout << "MaxSum = " << maxSum << endl;
    }

    return 0;
}