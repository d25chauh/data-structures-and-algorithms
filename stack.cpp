#include <iostream>
#include <stdio.h>
#include <stack>

using namespace std;

class Stack
{
private:
    int top;
    int arr[5];

public:
    Stack()
    {
        top = -1;
        for (int i = 0; i < 5; i++)
        {
            arr[i] = 0;
        }
    }
    bool isEmpty()
    {
        if (top == -1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    bool isFull()
    {
        if (top == 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void push(int val)
    {
        cout << "Current index is: " << top << endl;
        if (isFull())
        {
            cout << "Error: Statck Overflow" << endl;
        }
        else
        {
            top++;
            arr[top] = val;
            cout << "Element added to index: " << top << endl;
        }
    }

    int pop()
    {
        if (isEmpty())
        {
            cout << "Error: Stack is Empty. Stack Underflow." << endl;
            return 0;
        }
        else
        {
            int temp = arr[top];
            arr[top] = 0;
            top--;
            return temp;
        }
    }
    int count()
    {
        return (top + 1);
    }

    int peek(int pos)
    {
        if (isEmpty())
        {
            cout << "Stack Empty";
            return 0;
        }
        else
        {
            return (arr[pos]);
        }
    }

    void change(int pos, int val)
    {
        arr[pos] = val;
        cout << "Item changes at location " << pos << endl;
    }

    //Print
    void display()
    {
        cout << "All the elements in the stack are: " << endl;
        for (int i = 4; i >= 0; i--)
        {
            cout << arr[i] << endl;
        }
    }
};

int main()
{
    int op, position, value;
    Stack stack;
    do
    {
        cout << "Enter a operation (0 for exit):" << endl;
        cout << "1. Push" << endl;
        cout << "2. Pop" << endl;
        cout << "3. isEmpty()" << endl;
        cout << "4. isFull()" << endl;
        cout << "5. peek()" << endl;
        cout << "6. Change()" << endl;
        cout << "7. Count()" << endl;
        cout << "8. Display()" << endl;
        cout << "9. Clear Screen" << endl;

        cin >> op;

        switch (op)
        {
        case 0:
            break;
        case 1:
            cout << "Enter the val you want to push: ";
            cin >> value;
            stack.push(value);
            break;
        case 2:
            stack.pop();
            break;
        case 3:
            cout << stack.isEmpty() << endl;
            break;
        case 4:
            cout << stack.isFull() << endl;
            break;
        case 5:
            cout << "Enter the index you want to peek: ";
            cin >> position;
            stack.peek(position);
            break;
        case 6:
            cout << "Enter the position and value you want to change." << endl;
            cout << "Enter pos: ";
            cin >> position;
            cout << "Enter val: ";
            cin >> value;
            stack.change(position, value);
            break;
        case 7:
            cout << stack.count() << endl;
            break;
        case 8:
            stack.display();
            break;
        case 9:
            system("cls");
            break;
        default:
            cout << "Enter proper output number." << endl;
            break;
        }
    } while (op != 0);

    return 0;
}
