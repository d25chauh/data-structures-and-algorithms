#include <iostream>

using namespace std;

int main()
{
    int arr[5] = {3, 6, 7, 9, 2};

    int s = sizeof(arr) / sizeof(arr[0]);
    cout << "size(): " << s << endl;
    for (int i = 1; i < s; i++)
    {
        int key = arr[i];
        int j = i - 1;

        while (j >= 0 && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
    for (int i = 0; i < s; i++)
    {
        cout << arr[i] << " ";
    }
    return 0;
}