#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

//Singleton CLASS: The purpose of the singleton class is to control object creation, limiting the number of objects to only one.
class Admin
{
private:
    char name[20];
    char password[20];
    static int countOfObjects;

    //Private Constructor: You cannot create the object outside the class
    //You can create the object of the class inside a class function
    Admin()
    {
        strcpy(name, "Deepesh");
        strcpy(password, "Deepesh");
    }

public:
    void display()
    {
        cout << "Name: " << name << endl;
        cout << "Password: " << password << endl;
    }
    static Admin *getInstance()
    {
        if (countOfObjects == 0)
        {
            Admin *ptr = new Admin();
            countOfObjects++;
            return ptr;
        }
        else
        {
            return NULL;
        }
    }
};
int Admin::countOfObjects = 0;

int main()
{
    //Static function can be called without object
    Admin *p = Admin::getInstance();
    if (p != NULL)
    {
        p->display();
    }
    else
    {
        cout << "1: Only one object of class is allowed" << endl;
    }

    Admin *p1 = Admin::getInstance();
    if (p1 != NULL)
    {
        p1->display();
    }
    else
    {
        cout << "2: Only one object of class is allowed" << endl;
    }

    return 0;
}