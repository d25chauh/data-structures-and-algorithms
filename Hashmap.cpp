#include <iostream>
#include <iterator>
#include <map>

//Map: Implemented using Red Black Trees
//Map does NOT have continous memory location
// (Unique keys) KEYS: Object/premitive types || VALUES: Object/premitive types

using namespace std;

void printMap(map<int, string> &m1)
{
    cout << "Map size is: " << m1.size() << endl;

    for (auto &itr : m1)
    {
        //Time complexity for loop: n(logn)
        //Time complexity for accessing 1 element: (log(n))
        cout << itr.first << ":" << itr.second << endl;
    }
}

int main()
{
    map<int, string> m;
    m[1] = "Hello";                            //Time complexity : O(log(n))
    m.insert(pair<int, string>(2, "Deepesh")); //Time complexity: log(n)
    m.insert(pair<int, string>(3, "Lets do it!"));

    //Making an iterator
    map<int, string>::iterator it;

    for (it = m.begin(); it != m.end(); it++)
    {
        cout << it->first << " : " << it->second << endl;
        //cout << "2 = " << (*it).first << " : " << (*it).second << endl;
    }

    //Or we can also use a simpler version
    printMap(m);

    //find return an iterator to this memory block
    auto itr_find = m.find(4); //Time complexity: log(n)
    if (itr_find == m.end())
    {
        cout << "No value Found" << endl;
    }
    else
    {
        cout << (*itr_find).second << endl;
    }

    //Erase operation: Take 2 types of input: 1) Key 2) Iterator
    m.erase(3);
    printMap(m);

    auto itr_erase = m.find(2);
    m.erase(itr_erase);
    printMap(m);

    //Time Complexity also depends on the Keys: Example if the keys are of type string then,
    // [Time complexity] = s.size() * log(n)

    return 0;
}