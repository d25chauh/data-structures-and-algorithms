//Selection sort is in place comparison sort (A place is selected and compared to other places)
//Time complexity: O(n2)
//Divide the input list into 2 parts.
//Part1: Sublist of items that are already sorted AND Part2: Sublist of items remaining to be sorted
//Initially the sorted sublist (Part1) is empty and unsorted sublist (Part2) is the entire input list.
//The algorithm finds the smallest element in the unsorted sublist (Part2) and exchange it with the leftmost unsorted element.
//Then you Move the sublist boundary one element to the right.

#include <iostream>
#include <stdio.h>

using namespace std;

//By default passed by address. arr is just a pointer pointing to the base address of main array
void selectionSort(int arr[], int arr_size)
{
    for (int i = 0; i < arr_size - 1; i++)
    {
        int min = i;
        for (int j = i + 1; j < arr_size; j++)
        {
            if (arr[j] < arr[min])
            {
                min = j;
            }
        }
        if (min != i)
        {
            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }
    }
}

int main()
{

    int arr[5];
    cout << "Enter 5 elements: " << endl;
    int arr_size = sizeof(arr) / sizeof(arr[0]);
    cout << "Size of Array: " << arr_size << endl;

    for (int i = 0; i < arr_size; i++)
    {
        cin >> arr[i];
    }

    cout << "Unsorted Array:" << endl;
    for (int i = 0; i < arr_size; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;

    selectionSort(arr, arr_size);

    cout << "Srted Array:" << endl;
    for (int i = 0; i < arr_size; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;

    return 0;
}