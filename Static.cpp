#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;
//1. Static local variable : default 0 ; Memory is assigned only once at the beginning of the program
//2. Static Member Variable: Static Member variable in a class
//3. Static Member Function: Static Member Function in a class

class Account
{
private:
    int balance;
    static float roi; //Static member Variable || Class Variable = Class variable is not a part of the object -> No memory allocated to class var yet
public:
    void setBalance(int b)
    {
        balance = b;
    }
    static void set_ROI(float b) //Static Member Function
    {
        roi = b;
    }
};
//Now the static variable will get the memory-> Only when you define it
float Account::roi = 3.5f;

int main()
{
    Account::set_ROI(4.5f);
    return 0;
}